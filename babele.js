Hooks.once('init', () => {

    if(typeof Babele !== 'undefined') {
        Babele.get().register({
            module: 'symbaroum-packs-zh-tw',
            lang: 'zh-TW',
            dir: 'compendium'
        });
    }
});

